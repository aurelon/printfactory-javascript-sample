module.exports = {
  createDocument: createDocument
}

var https = require("https");

/**
 * @param {string} docJson json string of the document
 * @param {function(string) : void=} callback The callback function
 */
function createDocument (docJson, callback) {
  let options = {
    "method": "POST",
    "hostname": "api.aurelon.com",
    "path": "/api/v2/document/create",
    "headers": {
      "MisKey": MisKey,
      "Content-type": "application/json",
      "Content-Length": docJson.length
    }
  };

  let createRequest = https.request(options, function (res) {
    console.log(`statusCode: ${res.statusCode}`)

    var chunks = [];

    res.on("data", function (chunk) {
      chunks.push(chunk);
    });

    res.on("end", function () {
      let body = Buffer.concat(chunks);

      if (res.statusCode != 200) {
        throw body;
      }

      console.log(body.toString());

      if (callback instanceof (Function)) {
        callback(body);
      }
    });
  });

  createRequest.write(docJson);
}

// var docJson = `{
//   Name: "Doc Name",
//   Width: 194.175,
//   Height: 249.450
// }`;

// createDocument(docJson);