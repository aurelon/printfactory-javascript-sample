module.exports = {
    startUpExternalNotifications: startUpExternalNotifications
}
var WebSocket = require("nodejs-websocket") //you need to install this npm package using the following command: npm install nodejs-websocket

function startUpExternalNotifications (onMessage) {
    var ws = WebSocket.connect("ws://connect.aurelon.com:8080", {
        extraHeaders: {
            "MisKey": MisKey
        }
    });

    ws.on("connect", function () {
        console.log("External notifications connected");
    });
    ws.on("text", function (str) {
        if (onMessage instanceof(Function)){
            onMessage(str);
        }
        console.log("External notifications message: " + str);
    });
    ws.on("error", function (err) {
        console.log("External notifications error: " + err);
    });

    ws.on("close", function () {
        console.log("External notifications closed");
    });


}