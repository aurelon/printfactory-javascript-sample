module.exports = {
    setDocumentResource: setDocumentResource
}

var https = require("https");

function setDocumentResource (documentId, documentData, callback) {

    var options = {
        "method": "POST",
        "hostname": "api.aurelon.com",
        "path": `/api/v2/document/${documentId}`,
        "headers": {
            "MisKey": MisKey,
            "Content-type": "application/octet-stream",
            "Content-Length": documentData.length
        }
    };

    var setDocResourceReq = https.request(options, function (res) {
        console.log(`statusCode: ${res.statusCode}`)
        var chunks = [];

        res.on("data", function (chunk) {
            chunks.push(chunk);
        });

        res.on("end", function () {
            var body = Buffer.concat(chunks);

            if (res.statusCode != 200) {
                throw body;
            }

            //var jsonResp = JSON.parse(body);
            //documentId = jsonResp.DocumentGUID;
            console.log(body.toString());

            if (callback instanceof (Function)) {
                callback(body);
            }
        });
    });
    setDocResourceReq.write(documentData);
}

// var fs = require('fs');

// try {
//     var imageData = fs.readFileSync('c:/Users/User/Desktop/Api examples/Nodejs examples/doge.jpg');
//     console.log(imageData);
// } catch (e) {
//     console.log('Error:', e.stack);
// }

// var documentGUID = "e25d6884-e531-436c-847a-310713fb251e";
// setDocumentResource(documentGUID, imageData);