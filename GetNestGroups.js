module.exports = {
    get:GetNestingGroups
}

var https = require("https");

function GetNestingGroups(callback) {

    let options = {
        "method": "GET",
        "hostname": "api.aurelon.com",
        "path": "/api/v2/automation/nestgroups",
        "headers": {
            "MisKey": MisKey
        }
    };

    https.get(options, (res) => {
        console.log(`statusCode: ${res.statusCode}`)

        var chunks = [];

        res.on("data", function (chunk) {
            chunks.push(chunk);
        });

        res.on("end", function () {
            let body = Buffer.concat(chunks);

            if (res.statusCode != 200) {
                throw body;
            }

            console.log(body.toString());

            if (callback instanceof (Function)) {
                callback(body);
            }
        });

    }).on("error", (err) => {
        console.log("Error: " + err.message);
    });
}