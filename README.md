**Upload jobs to the nesting queue and receive tracking events**

This sample demonstrates creating a document, uploading the actual document or its thumbnail when you want to keep the file on premises and register to the websocket server to receive updates events on the progress of the processing.
Consult [help.printfactory.cloud](https://help.printfactory.cloud/knowledge-base/api/) for detailed explanation of the API and explanation on the usage scenarios referred to in this sample.

---

## Environment

The sample is developed based on node.js but can be used in other JavaScript enviroments as well, like Switch. The sample code expects the https and websocket modules to be available.

---

## Company details

To get started you will need to fill in the MisKey and other details of your own account at printfactory.cloud in main.js.
To retrieve your MisKey:

1. Log into [app.printfactory.cloud](https://app.printfactory.cloud).
2. Navigate to **Account (right menu) > Settings > External API**.
3. The **MIS Key** is shown on this page. When no MIS Key is shown then click on **Generate**.
4. Copy the key and fill it in at **global.MisKey** in **main.js**.

All API calls are pointing to the live environment. If your company has access to the acceptance environment then replace the calls to the set of servers given with your account.

---

For support and assistance a developer support plan is available. Please consult your sales representative for more information on the developer support plan.