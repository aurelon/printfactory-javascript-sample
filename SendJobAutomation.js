module.exports = {
    sendAutomationJob: sendAutomationJob
}

var https = require("https");

function sendAutomationJob (jobXml, callback) {
    let options = {
        "method": "POST",
        "hostname": "api.aurelon.com",
        "path": `/api/v2/automation`,
        "headers": {
            "MisKey": MisKey,
            "Content-type": "application/xml",
            "Content-Length": jobXml.length
        }
    };

    var setDocResourceReq = https.request(options, function (res) {
        console.log(`statusCode: ${res.statusCode}`)
        var chunks = [];

        res.on("data", function (chunk) {
            chunks.push(chunk);
        });

        res.on("end", function () {
            var body = Buffer.concat(chunks);

            if (res.statusCode != 200) {
                throw body;
            }

            //var jsonResp = JSON.parse(body);
            //documentId = jsonResp.DocumentGUID;
            console.log(body.toString());

            if (callback instanceof (Function)) {
                callback(body);
            }
        });
    });
    setDocResourceReq.write(jobXml);
}

// var nestGroupGUID ='aae9ce89-d922-4150-86a0-7fa78d9f21d9';
// var documentGUID = "e25d6884-e531-436c-847a-310713fb251e";
// var jobGUID = uuidv4();

// var jobXml = `<?xml version="1.0" encoding="UTF-8"?>
// <Job>
// <JobGUID>${jobGUID}</JobGUID>
// <Name>Valik example job</Name>
// <User>Marcus Wrong</User>
// <JobType>Regular</JobType>
// <DocumentGUID></DocumentGUID>
// <NestGroupGUID>${nestGroupGUID}</NestGroupGUID>
// <Print>true</Print>
// <PDFJob>
//     <InputJobs>
//         <FilePath Id="${documentGUID}" Copies="3">C:\\Users\\User\\Desktop\\Api examples\\Nodejs examples\\doge.jpg</FilePath>
//     </InputJobs>
//     <IsTemporary>false</IsTemporary>
// </PDFJob>
// </Job>`;

// sendAutomationJob(jobXml);

// function uuidv4() {
//     return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
//       var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
//       return v.toString(16);
//     });
//   }