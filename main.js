var createDocument = require("./CreateDocument");
var setDocResource = require("./SetDocResource");
var setDocThumbnail = require("./SetThumbnail");
var sendJobAutomation = require("./SendJobAutomation");
var externalNotifications = require("./ExternalNotifications");
var nestingGroups = require("./GetNestGroups");
var guiGen = require("./uuidv4.js");
var fs = require('fs');

global.MisKey = ""; //your MisKey here; used in other files as a global variable.
var documentPath = 'c:/Users/...'; //your document path here
var thumbnailPath = 'c:/Users/...'; //your thumbnail path here
var jobName = ""; // your job name here
var documentName = ""; //your document name here
var userName = ""; //your user name here
var nestGroupName = ""; //your nest group name here
var numberOfCopies = 1;  // Number of copies here.
                         // Alternatively create an imposition with multiple JobParts.
var jobGUID = guiGen.uuidv4();
var nestGroupGUID;

var onPremiseFiles = false;

if( !onPremiseFiles ) {
    try {
        var documentData = fs.readFileSync(documentPath);
        console.log("Document buffer: " + documentData);
    } catch (e) {
        console.log('Error:', e.stack);
    }

    var docObj = {
        Name: documentName
    }
}
else {
    // PNG representing the document when keeping the file on premise.
    // Also the thumbnail is option, it will make the web UI display frames instead of images
    try {
        var thumbnailData = fs.readFileSync(thumbnailPath);
        console.log("thumbnail buffer: " + thumbnailData);
    } catch (e) {
        console.log('Error:', e.stack);
    }

    //  Document dimensions from documentData or user input here.
    var documentDimension = {
        width: 100, //  Width of the document in units
        height: 200 //  Height of the document in units
    };

    var docObj = {
        Name: documentName,
        Width: documentDimension.width,
        Height: documentDimension.height
    }
}

var docJsonStr = JSON.stringify(docObj);

externalNotifications.startUpExternalNotifications(function (message) {
    var messageJson = JSON.parse(message);
    var jobGUID = messageJson.JobGUID;
});

var nestGroupsList;

nestingGroups.get(function(resp){
    let jsonResp = JSON.parse(resp);
    nestGroupsList = jsonResp;
    nestGroupGUID = nestGroupsList.find(g => g.Name == nestGroupName).Id;
});

createDocument.createDocument(docJsonStr, function (response) {
    let jsonResp = JSON.parse(response);
    let documentGUID = jsonResp.DocumentGUID;

    let jobXml = `<?xml version="1.0" encoding="UTF-8"?>
                    <Job>
                    <JobGUID>${jobGUID}</JobGUID>
                    <Name>${jobName}</Name>
                    <User>${userName}</User>
                    <JobType>Regular</JobType>
                    <NestGroupGUID>${nestGroupGUID}</NestGroupGUID>
                    <Print>true</Print>
                    <PDFJob>
                        <InputJobs>
                            <FilePath Id="${documentGUID}" Copies="${numberOfCopies}">${documentPath}</FilePath>
                        </InputJobs>
                        <IsTemporary>false</IsTemporary>
                    </PDFJob>
                    </Job>`;
    if( !onPremiseFiles ) {
        setDocResource.setDocumentResource(documentGUID, documentData, sendJobCallback);
    }
    else {
        setDocThumbnail.setDocumentThumbnail(documentGUID, thumbnailData, sendJobCallback);
    }
    
    function sendJobCallback(){
        sendJobAutomation.sendAutomationJob(jobXml);
    }
});
